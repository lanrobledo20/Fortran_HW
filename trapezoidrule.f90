program trapezoid1

! Setting variables
    implicit none
    integer, parameter :: n=10000
    real :: u
    real :: h
    integer :: i

    call trapezoid_integration(n, 50.0)

    contains
! end_val is the upper limit of the integral
       subroutine trapezoid_integration(n,end_val)
           implicit none
           integer :: n
           real :: end_val
           double precision :: integral
           integer :: i

! Start loop to implement Equation G.4
         integral = 0.0
         do i=0,n
! u defines the x-coordinate of each trapezoid for a given iteration
              u = (end_val*i)/n
! Computing the sum for trapezoid rule
              if ((i.eq.0).or.(i.eq.n)) then
                  integral = integral+integrand(u)
              else
                  integral = integral+(2.0*integrand(u))
              end if
          end do

          h=end_val/n
          integral = (h/2.0)*integral

          write (*,*) 'trapezoidal integration = ' , integral
        end subroutine trapezoid_integration

       ! Creating the function
                function integrand(x) result (value)
                  implicit none
                  real :: x
                  real :: value

                if (x .lt. 0.00001) then
                    x = 0.00001
        ! if "x" is less than (.lt.) 10^-5 then set x = 10^-5. This is to prevent
        ! the computer from trying to divide by zero for the first iteration.
                 end if

                  value = (x**4)*EXP(X)/((EXP(X)-1.0)**2)
                end function integrand

end program trapezoid1
